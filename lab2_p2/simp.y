%{
#include <stdio.h>
#include <stdlib.h>

void yyerror(const char *s) {
	printf("yyerror: %s",s);
	exit(-1);
}

extern FILE *   yyin;
%}

%token TBEGIN END DO ELSE IF INPUT PRINT THEN VAR WHILE PLUS MINUS STAR SLASH LBRA RBRA LT LTE EQ NEQ GTE GT ASSIGN SEMI INT ID

%start program

%%

program : declarations commands
declarations : | declaration SEMI declarations
declaration : VAR ID
commands : command | command SEMI commands                                                   
command: assign | if | while | block | input | print
assign: ID ASSIGN exp
if: IF condexp THEN command else
else: | ELSE command
while: WHILE condexp DO block
block: TBEGIN program END
input: INPUT ID
print: PRINT exp
condexp: exp comp exp
comp: EQ | NEQ | LTE | LT | GTE | GT
exp: term exp2
exp2: | PLUS exp | MINUS exp
term: factor term2
term2: | STAR term | SLASH term
factor: MINUS base | base
base: ID | INT | LBRA exp RBRA


%%

main(int argc,char ** argv)
{  
   if((yyin=fopen(argv[1],"r"))==NULL)
   {  printf("can't open %s\n",argv[1]);
      exit(0);
   }

   yyparse();
   fclose(yyin);

}

