#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "nodes.h"
#include "simp.tab.h"

extern void yyparse(void);
extern FILE *   yyin;
extern void showTree(NODE*,int);

NODE* ast;


main(int argc,char ** argv)
{
   if((yyin=fopen(argv[1],"r"))==NULL)
   {  printf("can't open %s\n",argv[1]);
      exit(0);
   }
   yyparse();
   showTree(ast,1);
   fclose(yyin);

}
