#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "nodes.h"
#include "simp.tab.h"

NODE* newInt(int v)
{
 NODE * n;
 n = (NODE *)malloc(sizeof(NODE));
 n->tag = INT;
 n->f.value = v;
 return n;
}

NODE * newId(char * i)
{  NODE * n;
   n = (NODE *)malloc(sizeof(NODE));
   n->tag = ID;
   n->f.id = i;
   return n;
}

NODE * newBinaryNode(NODE *n1,NODE *n2, int tag)
{  NODE * n;
   n = (NODE *)malloc(sizeof(NODE));
   n->tag = tag;
   n->f.b.n1 = n1;
   n->f.b.n2 = n2;
   return n;
}

NODE * newUnaryNode(NODE *n1, int tag)
{  
   return newBinaryNode(n1,NULL,tag);
}

NODE * newNode(int tag)
{  
   return newBinaryNode(NULL,NULL,tag);
}

char * showSymb(int symb)
{  switch(symb)
   {  case  TBEGIN: return "BEGIN";
      case  DO: return "DO";
      case  ELSE: return "ELSE";
      case  END: return "END";
      case  ID: return "ID";
      case  IF: return "IF";
      case  INT: return "INT";
      case  INPUT: return "INPUT";
      case  PRINT: return "PRINT";
      case  THEN: return "THEN";
      case  VAR: return "VAR";
      case  WHILE: return "WHILE";
      case  PLUS: return "+";
      case  MINUS: return "-";
      case  STAR: return "*";
      case  SLASH: return "/";
      case  LBRA: return "(";
      case  RBRA: return ")";
      case  LT: return "<";
      case  LTE: return "<=";
      case  EQ: return "=";
      case  NEQ: return "!=";
      case  GTE: return ">=";
      case  GT: return ">";
      case  ASSIGN: return ":=";
      case SEMI: return ";";
      case EOF: return "EOF";
      default: printf("bad symbol: %d",symb);
   }
}


showTree(NODE * tree,int depth)
{  int i;
   if(tree==NULL)
    return;
   for(i=0;i<depth;i++)
    putchar('-');
   switch(tree->tag)
   {  case ID: printf("%s\n",tree->f.id);
               return;
      case INT: printf("%d\n",tree->f.value);
               return;
      default: printf("%s\n",showSymb(tree->tag));
               showTree(tree->f.b.n1,depth+1);
               showTree(tree->f.b.n2,depth+1);
   }
}



