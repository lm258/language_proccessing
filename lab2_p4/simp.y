%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nodes.h"

extern FILE *yyin; 
NODE * ast; 


void yyerror(const char *s) {
	printf("yyerror: %s",s);
	exit(-1);
}

%}

%union {int num; char * id; NODE* node;}

%token TBEGIN END DO ELSE IF INPUT PRINT THEN VAR WHILE PLUS MINUS STAR SLASH LBRA RBRA LT LTE EQ NEQ GTE GT ASSIGN SEMI
%token <num> INT 
%token <id> ID

%type <node> program
%type <node> declaration
%type <node> declarations
%type <node> assign
%type <node> if
%type <node> base
%type <node> term
%type <node> factor
%type <node> exp
%type <node> condexp
%type <node> print
%type <node> input
%type <node> block
%type <node> while
%type <node> else
%type <node> command
%type <node> commands
%type <num> comp

%start program

%%

program : declarations commands { ast = newBinaryNode( $1 , $2 , SEMI ); }

declarations : { $$ = NULL; } | declaration SEMI declarations { $$ = newBinaryNode( $1 , $3 , SEMI ); }

declaration : VAR ID { char * tmp = strdup($2); $$ = newUnaryNode( newId(tmp) , VAR ); }

commands : command { $$ = $1; } 
	| command SEMI commands { $$ = newBinaryNode( $1 , $3 , SEMI ); }

command: assign { $$ = $1; } 
	| if { $$ = $1; } 
	| while { $$ = $1; } 
	| block { $$ = $1; } 
	| input { $$ = $1; } 
	| print { $$ = $1; }

assign: ID ASSIGN exp { char * tmp = strdup($1); $$ = newBinaryNode( newId(tmp) , $3 , ASSIGN ); }

if: IF condexp THEN command else { $$ = newBinaryNode( $2 , newBinaryNode( $4 , $5 , THEN ) , IF ); }

else: { $$ = NULL; } | ELSE command { $$ = newUnaryNode( $2 , ELSE ); }

while: WHILE condexp DO block { $$ = newBinaryNode( $2 , $4 , WHILE ); }

block: TBEGIN program END { $$ = $2; }

input: INPUT ID { char * tmp = strdup($2); $$ = newUnaryNode( newId(tmp) , INPUT ); }

print: PRINT exp { $$ = newUnaryNode( $2 ,PRINT ); }

condexp: exp comp exp { $$ = newBinaryNode( $1 , $3 , $2 ); }

comp: EQ { $$ = EQ; } 
	| NEQ { $$ = NEQ; } 
	| LTE { $$ = LTE; } 
	| LT { $$ = LT; } 
	| GTE { $$ = GTE; } 
	| GT { $$ = GT; }

exp: term PLUS exp 	{ $$ = newBinaryNode( $1 , $3 , PLUS ); } 
	| term MINUS exp { $$ = newBinaryNode( $1 , $3 , MINUS ); } 
	| term { $$ = $1; }

term: factor STAR term { $$ = newBinaryNode( $1 , $3 , STAR ); }
	| factor SLASH term  { $$ = newBinaryNode( $1 , $3 , SLASH ); }
	| factor { $$ = $1; }

factor: MINUS base { $$ = newUnaryNode( $2 ,MINUS); } 
	| base { $$ = $1; }

base: ID { char * tmp = strdup($1);  $$ = newId(tmp);} 
	| INT { $$ = newInt($1); } 
	| LBRA exp RBRA { $$ = $2; }


%%
