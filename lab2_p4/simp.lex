%{
#include <string.h>
#include "nodes.h"
#include "simp.tab.h"

%}

DIGIT    [0-9]
IDENT	[a-zA-Z][A-Za-z0-9]*
         
%%

";" 	{ return SEMI;}
":=" 	{ return ASSIGN;}
"IF"	{ return IF;}
"THEN"	{ return THEN;}
"ELSE"	{ return ELSE;}
"BEGIN"	{ return TBEGIN;}
"END"	{ return END;}
"WHILE" { return WHILE;}
"DO" 	{ return DO;}
"+"  	{return PLUS;}
"-"		{ return MINUS;}  
"INPUT"	{ return INPUT;}  
"PRINT"	{ return PRINT;} 
"VAR" 	{ return VAR;}
"*" 	{ return STAR;}
"/" 	{ return SLASH;}
"(" 	{ return LBRA;}
")" 	{ return RBRA;}
"<" 	{ return LT;}
"<=" 	{ return LTE;}
"==" 	{ return EQ;}
"!=" 	{ return NEQ;}
">" 	{ return GT;}
">=" 	{ return GTE;}
{DIGIT}+ {yylval.num = atoi(yytext); return INT;}
{IDENT}	 {yylval.id = strdup(yytext); return ID;}
<<EOF>>	 {return EOF;}

[ \t\n]+          /* eat up whitespace */


%%

int yywrap() { return EOF; }
