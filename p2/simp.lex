%{
#include "tokens.h"
%}

DIGIT    [0-9]
ID  	 [a-zA-Z]
         
%%

";" 	 {return SEMI;}
"DO"	 {return DO;}
"ELSE"	 {return ELSE;}
"IF"	 {return IF;}
"INT"	 {return INT;}
"INPUT"	 {return INPUT;}
"PRINT"	 {return PRINT;}
"THEN"	 {return THEN;}
"VAR"	 {return VAR;}
"WHILE"	 {return WHILE;}
"END"	 {return END;}
"+"	 	 {return PLUS;}
"-"		 {return MINUS;}
"*"	 	 {return STAR;}
"/"	  	 {return SLASH;}
"("	 	 {return LBRA;}
")"	 	 {return RBRA;}
"<"	 	 {return LT;}
"<="	 {return LTE;}
"="		 {return EQ;}
"!="	 {return NEQ;}
">="	 {return GTE;}
">"	 	 {return GT;}
":=" 	 {return ASSIGN;}
"BEGIN"  {return TBEGIN;}

{DIGIT}+ {return INT;}
{ID}+	 {return ID;}

<<EOF>>	 {return EOF;} /* end of file */
[ \t\n]+          /* eat up whitespace */


%%

int yywrap() { return EOF; }


