#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tokens.h"
#include "nodes.h"

extern int ch;
extern char * showSymb(int);
extern lex(FILE *);

extern NODE * program(FILE *);

extern printtree(NODE *,int);

#define MAXREG 10
#define E1 MAXREG+1
#define E2 MAXREG+2

char * registers[MAXREG];

struct functions{
  char * name;
  struct functions * next;
};

typedef struct functions functions;

functions * fun_list = NULL;

int rp;
int rb;

int labno;

isComp(int c)
{  switch(c)
   {  case LT: case LTE: case EQ:
      case GTE: case GT: case NEQ: return 1;
      default: return 0;
   }
}

char * notComp(int c)
{  switch(c)
   {  case LT: return "GE";
      case LTE: return "GT";
      case GTE: return "LT";
      case GT: return "LE";
      case EQ: return "NE";
      case NEQ: return "EQ";
      default: printf("code: unknown operator %s\n",showSymb(c));
               exit(0);
   }
}

char * showCode(int c)
{  switch(c)
   {  case PLUS: return "ADD";
      case MINUS: return "SUB";
      case STAR: return "MUL";
      case SLASH: return "UDIV";
      default: printf("code: unknown operator %s\n",showSymb(c));
               exit(0);
   }
}

showSource(NODE * t)
{  printf("; ");
   printtree(t,0);
   putchar('\n');
}

checkvar(char * id)
{  int i;
   for(i=rp-1;i>=rb;i--){
    if(  registers[i] != NULL && strcmp(id,registers[i])==0)
     return i+1;
    }
   return -1;
}

findvar(char * id)
{  int i;
   for(i=rp-1;i>=0;i--){
      if( registers[i] != NULL && strcmp(id,registers[i])==0)
       return i+1;
    }
   return -1;
}

addfunction( char * id ){
  if ( fun_list == NULL ){
    fun_list = (functions *)malloc( sizeof(functions) );
    fun_list->name = strdup(id);
  }else{
    functions * tmp = fun_list;
    fun_list = (functions *)malloc( sizeof(functions) );
    fun_list->name = strdup(id);
    fun_list->next = tmp;
  }
}

functions * findfunction( char * id ){
  functions * tmp = fun_list;

  while ( tmp != NULL ){
    if( strcmp( tmp->name , id ) == 0 )
      return tmp;
    
    tmp = tmp->next;
  }

  return NULL;
}


codeerror(NODE * tree,char * message)
{  printtree(tree,0);
   printf(" %s\n",message);
   exit(0);
}

codeprog(NODE * t)
{  printf("\tAREA ASMain,CODE\n");
   printf("__main\tPROC\n");
   printf("\tEXPORT __main\n");
   codeblock(t);
   printf("PLOOP\tB PLOOP\n");
   printf("\tENDP\n");
   printf("\tEND\n");  
}

codevar(NODE * t)
{  if(rp==MAXREG)
    codeerror(NULL,"too many variables");
   if(checkvar(t->f.b.n1->f.id)!=-1)
    codeerror(t,"declared already");
   registers[rp] = t->f.b.n1->f.id;
   printf("%s\tRN %d\n",registers[rp],rp+1);
   rp++;
}

codeassign(NODE * t)
{  int reg;
   reg = findvar(t->f.b.n1->f.id);
   if(reg==-1)
    codeerror(t->f.b.n1,"not declared2");

   if ( t->f.b.n2->tag == FUN_DEF ){
      codefun_def( reg , t->f.b.n2);
   }else{
      codeexp(reg,t->f.b.n2);
   }
}


codeif(NODE * t)
{  int ln;
   ln = labno;
   labno++;
   codeexp(0,t->f.b.n1);
   if(t->f.b.n2->tag==ELSE)
   {  printf("\tB%s IFALSE%d\n",notComp(t->f.b.n1->tag),ln);
      codetree(t->f.b.n2->f.b.n1);
      printf("\tB IFEND%d\n",ln);
      printf("IFALSE%d\n",ln);
      codetree(t->f.b.n2->f.b.n2);
   }
   else
   {  printf("\tB%s IFEND%d\n",notComp(t->f.b.n1->tag),ln);
      codetree(t->f.b.n2);
   }
   printf("IFEND%d\n",ln);
}

codewhile(NODE * t)
{  int ln;
   ln = labno;
   labno++;
   printf("WLOOP%d\n",ln);
   codeexp(0,t->f.b.n1);
   printf("\tB%s WEND%d\n",notComp(t->f.b.n1->tag),ln);
   codetree(t->f.b.n2);
   printf("\tB WLOOP%d\n",ln);
   printf("WEND%d\n",ln);
}

codeblock(NODE * t)
{  int rb1;
   rb1 = rb;
   rb = rp;
   codetree(t->f.b.n1);
   codetree(t->f.b.n2);
   rp = rb;
   rb = rb1;
}

codefun(NODE * t)
{
  int args = 0;

  //if its a function, then this is use declaring a function here
  if ( t->f.b.n1->f.b.n2 != NULL ){
    printf("%s\tPOP {R%d}\n" , t->f.b.n1->f.b.n1->f.id , args + 1 );
    addfunction(t->f.b.n1->f.b.n1->f.id);

    //pop out our arguments from the stack and into our registers
    NODE * tmp = t->f.b.n1->f.b.n2->f.b.n2;
    registers[args] = t->f.b.n1->f.b.n2->f.b.n1->f.id;

    while( tmp != NULL ){
      printf("\tPOP {R%d}\n" , (++args) );
      registers[args] = tmp->f.b.n1->f.id;
      tmp = tmp->f.b.n2;

      if ( args > 3 ){
        codeerror( t , "Too many function arguments" );
      }
    }

  }else{
    printf("%s" , t->f.b.n1->f.b.n1->f.id );
    addfunction(t->f.b.n1->f.b.n1->f.id);
  }

  int rb1 = rb;
  rb = 0;
  codeexp( 0 , t->f.b.n2 );
  printf("\tPUSH {R0}\n");
  rb =rb1;

  //print the proper exit code
  printf("\tBX LR\n");
}

codefun_def( int RD , NODE * t)
{
  NODE * tmp = t->f.b.n2;
  int args = 1;
  int reg = -1;

  while ( tmp != NULL ){
    if ( tmp->f.b.n1->tag != INT && tmp->f.b.n1->tag  != ID ){
      codeexp( rp + args , tmp->f.b.n1 );
      printf("\tPUSH {R%d}\n" , rp + args );
      args++;
    }else{
      reg = findvar( tmp->f.b.n1->f.id);

      if ( reg == -1 )
          codeerror( tmp ,"not declared");

      printf("\tPUSH {R%d}\n" , reg );
    }
    tmp = tmp->f.b.n2;
  }

  printf("\tBL %s\n" , t->f.b.n1->f.id );
  printf("\tPOP {R%d}\n" , RD);
}
  
codetree(NODE * t)
{  if(t==NULL)
    return;
   switch(t->tag)
   {  case SEMI: codetree(t->f.b.n1);
                 codetree(t->f.b.n2);
                 return;
      case VAR: codevar(t);
                showSource(t);
                return;
      case ASSIGN: codeassign(t);
                   showSource(t);
                   return; 
      case IF: codeif(t);
               return;
      case WHILE: codewhile(t);
                  return;
      case BEGIN: codeblock(t->f.b.n1);
                  return;
      case FUN: codefun(t);
                return;
      default: codeerror(t,"unknown construct");
               exit(0);
   }
}

codeexp(int RD,NODE * e)
{
   int reg;
   int tmp_e,tmp_e2;
   tmp_e = E1;
   tmp_e2 = E2;

   switch(e->tag)
   {  case ID: reg = findvar(e->f.id);
               if(reg==-1 )
                codeerror(e,"not declared");

                printf("\tMOV R%d,R%d\n",RD,reg);
               return;
      case INT: printf("\tMOV R%d,#0x%x\n",RD,e->f.value);
                return;
   }

   //segfaulting here
   if ( e->f.b.n1->tag == ID ){
    tmp_e = findvar( e->f.b.n1->f.id );
  }else{
    codeexp(tmp_e,e->f.b.n1);
  }

  if ( e->f.b.n2->tag == ID ){
    tmp_e2 = findvar( e->f.b.n2->f.id  );
  }

   if ( e->f.b.n2->tag != INT && e->f.b.n2->tag != FUN_DEF && e->f.b.n2->tag != ID ){
     printf("\tPUSH {R%d}\n",tmp_e);
     codeexp(tmp_e2,e->f.b.n2);
     printf("\tPOP {R%d}\n",tmp_e);
    }else if ( e->f.b.n2->tag == FUN_DEF ){
      codefun_def( tmp_e , e->f.b.n2 );
    }

  if(isComp(e->tag)){
    if ( e->f.b.n2->tag != INT ){
      printf("\tCMP R%d,R%d\n",tmp_e,tmp_e2);
    }else{
      printf("\tCMP R%d,#0x%x\n",tmp_e,e->f.b.n2->f.value);
    }
  }else{
    if ( e->f.b.n2->tag != INT ){
      printf("\t%s R%d,R%d,R%d\n",showCode(e->tag),RD,tmp_e,tmp_e2);
    }else{
      printf("\t%s R%d,R%d,#0x%x\n",showCode(e->tag),RD,tmp_e,e->f.b.n2->f.value);
    }
  }

  showSource(e);
}

main(int c,char ** argv)
{  FILE * fin;
   NODE * tree;
   
   if((fin=fopen(argv[1],"r"))==NULL)
   {  printf("can't open %s\n",argv[1]);
      exit(0);
   }
   ch = getc(fin);
   lex(fin);
   tree = program(fin);
   rb = 3;
   rp = 3;
   labno = 0;
   codeprog(tree);
}
