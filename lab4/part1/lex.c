#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tokens.h"

#define MAXIDENT 256

int ch;
int symb;
int value;
char id[MAXIDENT];
char tmp[MAXIDENT];

int cline; // current line

char * showSymb(int symb)
{  switch(symb)
   {  case  BEGIN: return "BEGIN";
      case  DO: return "DO";
      case  ELSE: return "ELSE";
      case  END: return "END";
      case  ID: return "ID";
      case  IF: return "IF";
      case  INT: return "INT";
      case  INPUT: return "INPUT";
      case  PRINT: return "PRINT";
      case  THEN: return "THEN";
      case  VAR: return "VAR";
      case  WHILE: return "WHILE";
      case  PLUS: return "+";
      case  MINUS: return "-";
      case  STAR: return "*";
      case  SLASH: return "/";
      case  LBRA: return "(";
      case  RBRA: return ")";
      case  LT: return "<";
      case  LTE: return "<=";
      case  EQ: return "=";
      case  NEQ: return "!=";
      case  GTE: return ">=";
      case  GT: return ">";
      case  ASSIGN: return ":=";
      case SEMI: return ";";
      case REPEAT: return "REPEAT";
      case UNTIL: return "UNTIL";
      case USING: return "USING";
      case IN: return "IN";
      case FUN: return "FUN";
      case COMMA: return ",";
      case APPF: return "APPF";
      case ARG: return "ARG";
      case FUN_DEF: return "FUN_DEF";
      case EOF: return "EOF";
            
      default: printf("bad symbol: %d",symb);
   }
}

printSymb()
{  char * s;
   printf("%s ",showSymb(symb));
   if(symb==ID)
    printf("%s\n",id);
   else
   if(symb==INT)
    printf("%d\n",value);
   else
   printf("\n");
}

char * getId(){
  return id;
}
   

getInteger(FILE * fin)
{  symb = INT;
   value=0;
   while(ch>='0' && ch<='9')
   {  value = 10*value+ch-'0';
      ch = getc(fin);
   }
}

getIdent(FILE * fin)
{ 
   int i;
   i = 0;
   while(( (ch=='_') || (ch>='a' && ch<='z') || (ch>='A' && ch<='Z') || 
          (ch>='0' && ch<='9')) && i<MAXIDENT-1)
   {  tmp[i] = ch;
      ch = getc(fin);
      i++;
   }
   tmp[i] = '\0';
   if(i==MAXIDENT-1)
    while((ch == '_') || (ch>='a' && ch<='z') || (ch>='A' && ch<='Z') ||
          (ch>='0' && ch<='9'))
     ch = getc(fin);
   if(strcmp(tmp,"BEGIN") == 0){
     symb = BEGIN;
     return;
   }   
   if(strcmp(tmp,"DO") == 0){
     symb = DO;
     return;
   } 
   if(strcmp(tmp,"ELSE") == 0){
     symb = ELSE;
     return;
   }    
   if(strcmp(tmp,"END") == 0){
     symb = END;
     return;
   }  
   if(strcmp(tmp,"IF") == 0){
     symb = END;
     return;
   }
   if(strcmp(tmp,"INPUT") == 0){
     symb = INPUT;
     return;
   }
   if(strcmp(tmp,"PRINT") == 0){
     symb = PRINT;
     return;
   }   
   if(strcmp(tmp,"THEN") == 0){
     symb = THEN;
     return;
   }   
   if(strcmp(tmp,"VAR") == 0){
     symb = VAR;
     return;
   }  
    if(strcmp(tmp,"WHILE") == 0){
     symb = WHILE;
     return;
   }    
    if(strcmp(tmp,"REPEAT") == 0){
     symb = REPEAT;
     return;
   }
    if(strcmp(tmp,"UNTIL") == 0){
     symb = UNTIL;
     return;
   }
    if(strcmp(tmp,"USING") == 0){
     symb = USING;
     return;
   }   
    if(strcmp(tmp,"IN") == 0){
     symb = IN;
     return;
   } 
    if(strcmp(tmp,"FUN") == 0){
     symb = FUN;
     return;
   }       
   
   strcpy(id,tmp);        
   symb = ID;
}
   

lex(FILE * fin)
{  while(ch==' ' || ch=='\n' || ch=='\t'){
  if(ch=='\n') cline++;
    ch = getc(fin);
    }
   switch(ch)
   {  case ';': symb = SEMI; break;
      case ',': symb=COMMA; break;
      case '+': symb = PLUS; break;
      case '-': symb = MINUS; break;
      case '*': symb = STAR; break;
      case '/': symb = SLASH; break;
      case '(': symb = LBRA; break;
      case ')': symb = RBRA; break;
      case '=': symb = EQ; break;
      case '!': ch = getc(fin);
                if(ch=='='){  symb = NEQ; break; }
                printf("lexical error on line %d: = expected after !\n",cline); exit(0);
      case ':': ch = getc(fin);
                if(ch=='='){  symb = ASSIGN; break;  }
                 printf("lexical error on line %d: = expected after :\n",cline); exit(0);
      case '<': ch = getc(fin);
                // if(ch=='-'){  symb = LARROW; break;  }
                if(ch=='='){  symb = LTE; break;  }
                symb = LT; return;
      case '>': ch = getc(fin);
                if(ch=='=')
                {  symb = GTE; break; }
                symb = GT; return;
      case EOF: symb = EOF; return;
      default: if(ch>='0' && ch<='9')
               {  getInteger(fin); return; }
               if((ch>='a' && ch<='z') || (ch>='A' && ch<='Z') || (ch == '_'))
               {  getIdent(fin); return;  }
               printf("lexical error on line %d: unknown character %c %x\n",cline,ch,ch);
               exit(0);
   }
   ch = getc(fin);
}
